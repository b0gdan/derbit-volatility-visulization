import hashlib, time, base64
import json


def get_signature(action, arguments):
    nonce = str(int(time.time() * 1000))

    with open('api.json') as json_file:
        api_dict = json.load(json_file)

        signature_string = '_=%s&_ackey=%s&_acsec=%s&_action=%s' % (
            nonce, api_dict['client_id'], api_dict['clien_secret'], action
        )

        for key, value in sorted(arguments.items()):
            if isinstance(value, list):
                value = "".join(str(v) for v in value)
            else:
                value = str(value)

            signature_string += "&%s=%s" % (key, value)

        sha256 = hashlib.sha256()
        sha256.update(signature_string.encode("utf-8"))
        signature_hash = base64.b64encode(sha256.digest()).decode()

        return "%s.%s.%s" % (api_dict['client_id'], nonce, signature_hash)


